FROM node:8-stretch

WORKDIR /simple-app
COPY ./ ./

RUN yarn install
RUN yarn global add typescript
RUN yarn build

ENTRYPOINT [ "node" , "dist/app.js"]