import * as express from  'express'

const app = express();
const port = 3000;

app.get('/', (req , res) => {
    res.send("API Result")
})
app.get('/health', (req, res) => {
    res.send("OK")
})

app.listen(port, err => {
    if (err) {
        return console.error(err)
    }
    return console.log(`Server is listening to port ${port}`)
})